module Utils where

allValues :: (Bounded a, Enum a) => [a]
allValues = [minBound ..]

infixl 1 |>
(|>) :: (Functor m) => m a -> (a -> b) -> m b
(|>) = flip (<$>)
