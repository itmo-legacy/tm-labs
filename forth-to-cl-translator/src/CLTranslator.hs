{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}

module CLTranslator (translate) where

import           Control.Monad     (foldM)
import           Data.Function     ((&))
import qualified Data.HashSet      as S
import           Data.Semigroup    ((<>))
import           Data.Text         (Text)
import qualified Data.Text         as T
import           NeatInterpolation (text)
import           Text.Read         (readMaybe)

import           Ast               (Ast (..), Builtin (..), Operation (..),
                                    Std (..), toBuiltin)
import           Utils             (allValues, (|>))


encodeForthWord :: Text -> Text
encodeForthWord = T.append "_forth/"

encodeHelper :: Text -> Text
encodeHelper = id

data Helper = PushfL | StackDefunL deriving (Bounded, Enum)

helperName :: Helper -> Text
helperName = encodeHelper . name
  where name PushfL      = "pushf"
        name StackDefunL = "stack-defun"

helperBody :: Helper -> Text
helperBody helper = body (helperName helper) helper
  where body name PushfL = [text|(defmacro $name (item stack) `(setf ,stack (push ,item ,stack)))|]
        body name StackDefunL = [text|
            (defmacro $name (fname arity fun)
              `(defmacro ,fname (stack)
                `(let* ((args (reverse (loop for i from 1 to ,,arity collect (pop ,stack))))
                        (result (apply ,,fun args)))
                    (if (listp result)
                        (setf ,stack (nconc result ,stack))
                        ($pushf result ,stack)))))
                                      |]
        pushf = helperName PushfL

stdName :: Std -> Text
stdName = encodeForthWord . name
  where name DupW    = "dup"
        name DropW   = "drop"
        name SwapW   = "swap"
        name PlusW   = "+"
        name StarW   = "*"
        name MinusW  = "-"
        name DotW    = "."
        name CrW     = "cr"
        name LtW     = "<"
        name EqW     = "="
        name GtW     = ">"
        name NeW     = "<>"
        name AndW    = "and"
        name OrW     = "or"
        name InvertW = "invert"
        name NegateW = "negate"

stdBody :: Std -> Text
stdBody std = body (stdName std) std
  where body name DupW    = [text|(defmacro $name (stack) `($pushf (car ,stack) ,stack))|]
        body name DropW   = [text|(defmacro $name (stack) `(pop ,stack))|]
        body name SwapW   = [text|(defun $name (stack) (rotatef (car stack) (cadr stack)))|]
        body name PlusW   = [text|($stackDefun $name 2 #'+)|]
        body name StarW   = [text|($stackDefun $name 2 #'*)|]
        body name MinusW  = [text|($stackDefun $name 2 #'-)|]
        body name DotW    = [text|($stackDefun $name 1 (lambda (n) (format t "~A " n)))|]
        body name CrW     = [text|($stackDefun $name 0 #'terpri)|]
        body name LtW     = [text|($stackDefun $name 2 (lambda (a b) (if (< a b) -1 0)))|]
        body name EqW     = [text|($stackDefun $name 2 (lambda (a b) (if (= a b) -1 0)))|]
        body name GtW     = [text|($stackDefun $name 2 (lambda (a b) (if (> a b) -1 0)))|]
        body name NeW     = [text|($stackDefun $name 2 (lambda (a b) (if (/= a b) -1 0)))|]
        body name AndW    = [text|($stackDefun $name 2 #'logand)|]
        body name OrW     = [text|($stackDefun $name 2 #'logior)|]
        body name InvertW = [text|($stackDefun $name 1 #'lognot)|]
        body name NegateW = [text|($stackDefun $name 1 #'-)|]

        pushf = helperName PushfL
        stackDefun = helperName StackDefunL

defaultStackName :: Text
defaultStackName = "*stack*"

parameterStackName :: Text
parameterStackName = "stack"

-- type Context = S.HashSet Text

data Context = Context
             { ctxtDict   :: S.HashSet Text
             , ctxtOffset :: Text
             , ctxtStack  :: Text
             }

translate :: Ast -> Either Text Text
translate = translateWithInit init'
  where init' = (initText, Context stdDict "" defaultStackName)
        initText = T.concat [";;; LIBRARY ;;;\n\n", T.unlines allDecls, "\n;;; PROGRAM ;;;\n\n"]
        stdDict = S.fromList (map stdName allValues)
        allDecls = helperDecls ++ stdDecls ++ [stackDecl]
        stackDecl = [text|(defvar ${defaultStackName} '())|]
        helperDecls = map helperBody allValues
        stdDecls = map stdBody allValues


translateWithInit :: (Text, Context) -> Ast -> Either Text Text
translateWithInit init' (Ast ops) = foldM accF init' ops |> fst
  where accF (acc, ctxt) (Word op)
          = do (tr, ctxt') <- (translateNormal <> translateBuiltin <> translateNumber) op ctxt
               return (T.append acc tr, ctxt')

        accF (acc, ctxt@Context{ ctxtOffset = offset }) (StrOut s)
          = let tr = [text|${offset}(princ "$s")|]
            in return (T.append acc tr, ctxt)

        accF (acc, Context dict offset stack) (Definition ident ast)
          = do let offset' = incOffset offset
                   offset'' = incOffset offset'
                   forthIdent = encodeForthWord ident
               body <- translateWithInit ("", Context dict offset'' (T.append "," parameterStackName)) ast
               let tr = [text|${offset}(defmacro $forthIdent (${parameterStackName})
                              ${offset'}`(progn
                               ${body}))|]
               return (T.append acc tr, Context (S.insert forthIdent dict) offset stack)

type Translation = Text -> Context -> Either Text (Text, Context)

translateNormal :: Translation
translateNormal raw ctxt@(Context dict offset stackName)
  | S.member s dict = Right ([text|${offset}($s ${stackName})|], ctxt)
  | otherwise = Left [text|Word $s is not declared|]
      where s = encodeForthWord raw

translateNumber :: Translation
translateNumber s ctxt@Context { ctxtOffset = offset, ctxtStack = stackName }
  | Just (_ :: Int) <- readMaybe $ T.unpack s = Right ([text|${offset}($pushf $s ${stackName})|], ctxt)
  | otherwise = Left [text|'${s}' is not in a dictionary, a builtin command or a number|]
      where pushf = helperName PushfL

translateBuiltin :: Translation
translateBuiltin s ctxt@Context { ctxtOffset = offset, ctxtStack = stackName }
  = toBuiltin s & maybe (Left [text|'${s}' is not in a dictionary, nor is it a builtin command|])
                        (Right . translate')
  where translate' IfW =
          let tr = [text|${offset}(if (/= 0 (pop ${stackName}))
                         ${offset'}(progn|]
              offset' = incOffset offset
          in (tr, ctxt{ ctxtOffset = incOffset offset' })
        translate' ElseW =
          let tr = [text|${offset'})
                         ${offset'}(progn|]
              offset' = decOffset offset
          in (tr, ctxt)
        translate' ThenW =
          let tr = [text|${offset'}))|]
              offset' = decOffset offset
          in (tr, ctxt{ ctxtOffset = decOffset offset'})

incOffset :: Text -> Text
incOffset = T.append "  "

decOffset :: Text -> Text
decOffset = T.drop 2
