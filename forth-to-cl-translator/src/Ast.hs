{-# LANGUAGE OverloadedStrings #-}

module Ast (Ast(..), Operation(..), Builtin(..), Std(..), toBuiltin) where

import           Data.Text (Text)


newtype Ast = Ast [Operation]
data Operation = Word Text | StrOut Text | Definition Text Ast
data Builtin = IfW | ThenW | ElseW
data Std
  = DupW
  | DropW
  | SwapW
  | PlusW
  | StarW
  | MinusW
  | DotW
  | CrW
  | LtW
  | EqW
  | GtW
  | NeW
  | AndW
  | OrW
  | InvertW
  | NegateW
  deriving (Bounded, Enum)

toBuiltin :: Text -> Maybe Builtin
toBuiltin "if"   = Just IfW
toBuiltin "then" = Just ThenW
toBuiltin "else" = Just ElseW
toBuiltin _      = Nothing
