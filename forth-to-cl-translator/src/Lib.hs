module Lib (translateForthToCL) where

import           Data.Function ((&))
import           Data.Text (Text)
import qualified Data.Text.Lazy as TL

import           CLTranslator (translate)
import           Lexer (dyslexScanTokens)
import           Parser (parseForth)

translateForthToCL :: Text -> Either Text Text
translateForthToCL dsl = TL.fromStrict dsl
                       & dyslexScanTokens
                     >>= parseForth
                       & either (Left . TL.toStrict) Right
                     >>= translate
