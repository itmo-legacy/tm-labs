{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Data.Text.IO as T
import qualified Data.Text as T
import Control.Arrow ((>>>))

import Lib (translateForthToCL)

main :: IO ()
main = T.interact (translateForthToCL >>> either (T.append "ERROR!!\n") id)
