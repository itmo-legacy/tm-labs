;;; LIBRARY ;;;

(defmacro pushf (item stack) `(setf ,stack (push ,item ,stack)))

(defmacro stack-defun (fname arity fun)
  `(defmacro ,fname (stack)
    `(let* ((args (reverse (loop for i from 1 to ,,arity collect (pop ,stack))))
            (result (apply ,,fun args)))
        (if (listp result)
            (setf ,stack (nconc result ,stack))
            (pushf result ,stack)))))

(defmacro _forth/dup (stack) `(pushf (car ,stack) ,stack))

(defmacro _forth/drop (stack) `(pop ,stack))

(defun _forth/swap (stack) (rotatef (car stack) (cadr stack)))

(stack-defun _forth/+ 2 #'+)

(stack-defun _forth/* 2 #'*)

(stack-defun _forth/- 2 #'-)

(stack-defun _forth/. 1 (lambda (n) (format t "~A " n)))

(stack-defun _forth/cr 0 #'terpri)

(stack-defun _forth/< 2 (lambda (a b) (if (< a b) -1 0)))

(stack-defun _forth/= 2 (lambda (a b) (if (= a b) -1 0)))

(stack-defun _forth/> 2 (lambda (a b) (if (> a b) -1 0)))

(stack-defun _forth/<> 2 (lambda (a b) (if (/= a b) -1 0)))

(stack-defun _forth/and 2 #'logand)

(stack-defun _forth/or 2 #'logior)

(stack-defun _forth/invert 1 #'lognot)

(defvar *stack* '())


;;; PROGRAM ;;;

(defmacro _forth/odd (stack)
  `(progn
     (_forth/dup ,stack)
     (pushf 1 ,stack)
     (_forth/and ,stack)
     (_forth/swap ,stack)
     (_forth/swap ,stack)
     (if (/= 0 (pop ,stack))
       (progn
         (_forth/dup ,stack)
         (_forth/. ,stack)
         (princ "is indeed odd!")
       )
       (progn
         (_forth/dup ,stack)
         (_forth/. ,stack)
         (princ "is totally even.")
       ))
     (_forth/cr ,stack)))
(pushf 5 *stack*)
(_forth/odd *stack*)
(pushf 6 *stack*)
(_forth/odd *stack*)
(pushf 0 *stack*)
(_forth/odd *stack*)
(pushf -1 *stack*)
(_forth/odd *stack*)
