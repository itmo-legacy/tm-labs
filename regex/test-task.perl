#! /usr/bin/env -S perl -w

use v5.24;

use strict;
use warnings;

my $task_number = shift @ARGV or die "Usage: $0 task-number\n";
my $file_template = "task${task_number}";
my $task_file = "${file_template}.perl";
my $test_file = "${file_template}.test";
my $gold_file = "${file_template}.gold";

my $diff = `bash -c 'diff <(perl $task_file $test_file) $gold_file' 2>&1`;
if ($diff eq "") {
    print "OK";
} else {
    print "ERR\n$diff";
}
