#! /usr/bin/env -S perl -w

use strict;
use warnings;

use constant
    TASK_TEMPLATE => qq{#! /usr/bin/env -S perl -w

use strict;
use warnings;

while (<>) {
}
};

my $task_number = shift @ARGV or die "Usage: $0 task-number\n";
my $file_template = "task${task_number}";
my $task_file = "${file_template}.perl";
my $test_file = "${file_template}.test";
my $gold_file = "${file_template}.gold";

system "touch $task_file $test_file $gold_file";
system "echo '" . TASK_TEMPLATE . "' >> $task_file";
