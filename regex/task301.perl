#! /usr/bin/env -S perl -w

use strict;
use warnings;

my $contents;

{
    local $/;
    $contents = <>;
}

# remove empty lines at the beginning and at the end of input
$contents =~
    s/
      \A
      ^\s*$(?)\n # empty lines at the start
      ( # text
        (^.*$(?)\n)*
        ^.*\S.*$
      )\n?
      ^\s*$(?)\n? # empty lines at the end
      \Z
    /$1/mx;

# collapse empty lines
$contents =~ s/^\s*$(?)\n?/\n/mg;

# remove whitespaces at the beginning and at the end of each line
$contents =~ s/^\h*(.*\S)\h*$/$1/mg;

# collapse whitespaces
$contents =~ s/\h+/ /mg;

print $contents;
