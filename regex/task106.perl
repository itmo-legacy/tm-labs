#! /usr/bin/env -S perl -w

use strict;
use warnings;

while (<>) {
    print if /\b\d+\b/;
}

