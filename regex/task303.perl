#! /usr/bin/env -S perl -w

use v5.24;

use strict;
use warnings;

sub extract_site {
    my $link = shift;
    if ($link =~ m|^(^\w+:)(//)?([^:/?\#]+)([/?\#].*)?$|) {
        return (1, $3);
    } elsif ($link =~ m|^([^:/?\#]+)(:\d+)([/?\#].*)?$|) {
        return (1, $1);
    } else {
        return (0, undef);
    }
}

my %sites;

my $contents;
{
    local $/;
    $contents = <>;
}

$contents =~ s/\v//g;

while ($contents =~ /<a.*? href\s*=\s*("|')?(.*?)\1.*?>/g) {
    my ($ok, $site) = extract_site($2);
    next if !$ok;
    $sites{$site} = undef;
}

say for sort keys %sites
