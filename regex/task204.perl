#! /usr/bin/env -S perl -w

use strict;
use warnings;

while (<>) {
    s/(\w+)(\W+)(\w+)/$3$2$1/;
    print;
}

