#! /usr/bin/env -S perl -w

use strict;
use warnings;

while (<>) {
    s/\b(\w)(\w)/$2$1/g;
    print;
}

