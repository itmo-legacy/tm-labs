#! /usr/bin/env -S perl -w

use strict;
use warnings;

sub format_text {
    my $contents = shift;
    return $contents
        # remove empty lines at the beginning and at the end of input
        =~ s/
             \A
             ^\s*$(?)\n # empty lines at the start
             ( # text
             (^.*$(?)\n)*
             ^.*\S.*$
             )\n?
             ^\s*$(?)\n? # empty lines at the end
             \Z
           /$1/mxr
        # collapse empty lines
        =~ s/^\s*$(?)\n?/\n/mgr
        # remove whitespaces at the beginning and at the end of each line
        =~ s/^\h*(.*\S)\h*$/$1/mgr
        # collapse whitespaces
        =~ s/\h+/ /mgr;
}

my $contents;

{
    local $/;
    $contents = <>;
}

$contents =~ s/<[^>]*>//mg;

print format_text($contents);
