#! /usr/bin/env -S perl -w

use strict;
use warnings;

while (<>) {
    s/(\p{L})\1+/$1/g;
    print;
}

