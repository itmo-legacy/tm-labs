{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms   #-}
{-# LANGUAGE ViewPatterns      #-}

module OldLexer (Token(..), tokenize) where

import           Control.Arrow (first)
import qualified Data.ByteString.Char8 as BS
import           Data.Char (isSpace)
import           Data.Function ((&))

import           Stream (pattern (:<), pattern Nil)
import           Utils ((|>))

data Token
    = PlusT
    | MinusT
    | StarT
    | OpParenT
    | ClParenT
    | ValT Int
    deriving (Eq, Show)

tokenize :: BS.ByteString -> Either String [Token]
tokenize Nil = return []
tokenize ('+':<cs) = (PlusT :) <$> tokenize cs
tokenize ('-':<cs) = (MinusT :) <$> tokenize cs
tokenize ('*':<cs) = (StarT :) <$> tokenize cs
tokenize ('(':<cs) = (OpParenT :) <$> tokenize cs
tokenize (')':<cs) = (ClParenT :) <$> tokenize cs
tokenize (lexVal -> Just (val, rest)) = (val :) <$> tokenize rest
tokenize (c:<cs) | isSpace c = cs & BS.span isSpace & snd & tokenize
tokenize (c:<cs) = Left ("Unexpected symbol: '" ++ [c] ++ "', followed by '" ++ BS.unpack cs ++ "'")

lexVal :: BS.ByteString -> Maybe (Token, BS.ByteString)
lexVal s = BS.readInt s |> first ValT
