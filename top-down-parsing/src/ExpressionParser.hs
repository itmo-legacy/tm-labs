module ExpressionParser (expressionP, runP, Expression) where

import           Control.Applicative ((<|>))
import           Data.Functor        (($>))

import           Expression          (Expression (..))
import           Lexer               (Token (..))
import           OldParser           (Parser (runP), chainl1, single, token)

type TParser = Parser [Token]

expressionP :: TParser Expression
expressionP = chainl1 termP plusOrMinusP
  where plusOrMinusP = single PlusT $> (:+) <|> single MinusT $> (:-)

termP :: TParser Expression
termP = chainl1 factorP (single StarT $> (:*))

factorP :: TParser Expression
factorP = negationP <|> parensP <|> valueP
  where negationP = (single MinusT $> Neg) <*> factorP
        parensP = single OpParenT *> expressionP <* single ClParenT

valueP :: TParser Expression
valueP = token getX
  where getX (ValT x) = Just (Val x)
        getX _        = Nothing
