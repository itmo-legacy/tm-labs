module Graph(ExprGraph, exprGraphParams, makeExprGraph) where

import           Data.Function ((&))
import           Data.GraphViz as G

import           Expression    (Expression (..))

type V = (Int, String)
type E = (Int, Int, ())

type ExprGraph = ([V], [E])

exprGraphParams :: G.GraphvizParams Int String () () String
exprGraphParams = G.nonClusteredParams {G.fmtNode = \(_, vl) -> [G.toLabel vl]}

makeExprGraph :: Expression -> ExprGraph
makeExprGraph ast = go 0 ast & \(_, ns, es) -> (ns, es)
  where go i (a :+ b) = process i "+" a b
        go i (a :- b) = process i "-" a b
        go i (a :* b) = process i "*" a b
        go i (a :^ b) = process i "^" a b
        go i (Neg a) =
          let (i', ns, es) = go (i + 1) a
          in (i', (i, "neg"):ns, (i, i + 1, ()):es)
        go i (Val v) = (i + 1, [(i, show v)], [])

        process i s a b =
          let (i1, ns1, es1) = go (i + 1) a
              (i2, ns2, es2) = go i1 b
              ns = ns1 ++ ns2 ++ [(i, s)]
              es = es1 ++ es2 ++ [(i, i + 1, ()), (i, i1, ())]
          in (i2, ns, es)
