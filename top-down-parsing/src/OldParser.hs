{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE InstanceSigs     #-}
{-# LANGUAGE PatternSynonyms  #-}
{-# LANGUAGE RankNTypes       #-}

module OldParser (Parser(..), ok, eof, token, satisfy, single, stream, chainl1, chainr1) where

import           Control.Applicative hiding (empty)
import qualified Control.Applicative as A (Alternative (empty))
import           Control.Arrow       (first, (>>>))
import           Control.Monad       ((>=>))
import           Data.Foldable       (foldl')
import           Data.Function       ((&))

import           Stream              (pattern (:<), pattern Nil, Stream (..))
import           Utils               ((|>))

newtype Parser s a = Parser { runP :: Stream s => s -> Maybe (a, s) }

instance Functor (Parser s) where
  fmap :: (a -> b) -> Parser s a -> Parser s b
  fmap f (Parser p) = Parser (p >>> fmap (first f))

instance Applicative (Parser s) where
  pure :: a -> Parser s a
  pure a = Parser $ \s -> Just (a, s)

  (<*>) :: Parser s (a -> b) -> Parser s a -> Parser s b
  Parser pf <*> Parser pa = Parser $ \s -> do
    (f, t) <- pf s
    (a, r) <- pa t
    return (f a, r)

instance Monad (Parser s) where
  (>>=) :: Parser s a -> (a -> Parser s b) -> Parser s b
  Parser pa >>= f = Parser $ pa >=> (\(a, t) -> runP (f a) t)

instance Alternative (Parser s) where
  empty :: Parser s a
  empty = Parser $ const Nothing

  (<|>) :: Parser s a -> Parser s a -> Parser s a
  Parser pa1 <|> Parser pa2 = Parser $ \s -> pa1 s <|> pa2 s

ok :: Monoid a => Parser s a
ok = Parser $ \s -> Just (mempty, s)

eof :: Parser s ()
eof = Parser $ \s -> case s of
  Nil -> Just ((), Nil)
  _   -> Nothing

token :: Stream s => (Token s -> Maybe a) -> Parser s a
token f = Parser $ \s -> case s of
  Nil     -> Nothing
  (c:<cs) -> f c |> \x -> (x, cs)

satisfy :: Stream s => (Token s -> Bool) -> Parser s (Token s)
satisfy p = token (\t -> if p t then Just t else Nothing)

single :: (Stream s, Eq (Token s)) => Token s -> Parser s (Token s)
single c = satisfy (== c)

stream :: (Stream s, Eq (Token s)) => s -> Parser s s
stream Nil     = Parser $ \s -> Just (Nil, s)
stream (c:<cs) = cons <$> single c <*> stream cs

chainr1 :: Parser s b -> Parser s (b -> b -> b) -> Parser s b
chainr1 term op = do
    init' <- many (term <**> op)
    z <- term
    return $ foldr ($) z init'

chainl1 :: Parser s b -> Parser s (b -> b -> b) -> Parser s b
chainl1 term op = do
    a <- term
    rest <- many (flip <$> op <*> term)
    return $ foldl' (&) a rest
