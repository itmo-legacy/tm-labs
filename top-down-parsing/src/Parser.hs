{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}
{-# OPTIONS_GHC -fno-warn-unused-matches #-}


module Parser where

import Data.Text.Lazy (unpack)

import           Expression          (Expression (..))
import           Lexer               (Token (..))

import qualified Data.Text.Lazy as T (Text, pack)
import Debug.Trace (trace)


newtype Wrapper a b = Wrapper {unwrap :: [Wrapper a b] -> [a] -> [a] -> Either T.Text b}
wrap = Wrapper

data ParsedItem t0 t1 t2 t3 t4 t5 = PIToken ( Token ) | PINT0 t0 | PINT1 t1 | PINT2 t2 | PINT3 t3 | PINT4 t4 | PINT5 t5

parseArithmetics tokens = goOn [wrap state_0] (map PIToken tokens) []

goOn s_stk@(s : _) t_stk i_stk = unwrap s s_stk t_stk i_stk
goOn _ _ _ = Left (T.pack "can't keep on parsing")

state_0 [_] [PINT0 _] [PINT0 v] = return v
state_0 s_stk (tok@(PIToken ( MinusT )) : t_stk) i_stk = trace "state0, shift5" $ goOn (wrap state_5 : s_stk) t_stk (tok : i_stk)
state_0 s_stk (tok@(PIToken ( OpParenT )) : t_stk) i_stk = trace "state0, shift6" $ goOn (wrap state_6 : s_stk) t_stk (tok : i_stk)
state_0 s_stk (tok@(PIToken ( ValT _ )) : t_stk) i_stk = trace "state0, shift7" $ goOn (wrap state_7 : s_stk) t_stk (tok : i_stk)
state_0 s_stk (PINT1 _ : t_stk) i_stk = trace "state0, goto1" $ goOn (wrap state_1 : s_stk) t_stk i_stk
state_0 s_stk (PINT2 _ : t_stk) i_stk = trace "state0, goto2" $ goOn (wrap state_2 : s_stk) t_stk i_stk
state_0 s_stk (PINT3 _ : t_stk) i_stk = trace "state0, goto3" $ goOn (wrap state_3 : s_stk) t_stk i_stk
state_0 s_stk (PINT4 _ : t_stk) i_stk = trace "state0, goto4" $ goOn (wrap state_4 : s_stk) t_stk i_stk
state_0 _ _ _ = Left "no transition in state 0"

state_1 s_stk (tok@(PIToken ( PlusT )) : t_stk) i_stk = trace "state1, shift8" $ goOn (wrap state_8 : s_stk) t_stk (tok : i_stk)
state_1 s_stk (tok@(PIToken ( MinusT )) : t_stk) i_stk = trace "state1, shift9" $ goOn (wrap state_9 : s_stk) t_stk (tok : i_stk)
state_1 s_stk t_stk@[] i_stk = reduce_0 s_stk t_stk i_stk
state_1 _ _ _ = Left "no transition in state 1"

state_2 s_stk (tok@(PIToken ( StarT )) : t_stk) i_stk = trace "state2, shift10" $ goOn (wrap state_10 : s_stk) t_stk (tok : i_stk)
state_2 s_stk t_stk@[] i_stk = reduce_3 s_stk t_stk i_stk
state_2 s_stk t_stk@(PIToken ( PlusT ) : _) i_stk = reduce_3 s_stk t_stk i_stk
state_2 s_stk t_stk@(PIToken ( ClParenT ) : _) i_stk = reduce_3 s_stk t_stk i_stk
state_2 s_stk t_stk@(PIToken ( MinusT ) : _) i_stk = reduce_3 s_stk t_stk i_stk
state_2 _ _ _ = Left "no transition in state 2"

state_3 s_stk t_stk@[] i_stk = reduce_5 s_stk t_stk i_stk
state_3 s_stk t_stk@(PIToken ( PlusT ) : _) i_stk = reduce_5 s_stk t_stk i_stk
state_3 s_stk t_stk@(PIToken ( ClParenT ) : _) i_stk = reduce_5 s_stk t_stk i_stk
state_3 s_stk t_stk@(PIToken ( MinusT ) : _) i_stk = reduce_5 s_stk t_stk i_stk
state_3 s_stk t_stk@(PIToken ( StarT ) : _) i_stk = reduce_5 s_stk t_stk i_stk
state_3 _ _ _ = Left "no transition in state 3"

state_4 s_stk (tok@(PIToken ( HatT )) : t_stk) i_stk = trace "state4, shift11" $ goOn (wrap state_11 : s_stk) t_stk (tok : i_stk)
state_4 s_stk t_stk@[] i_stk = reduce_7 s_stk t_stk i_stk
state_4 s_stk t_stk@(PIToken ( PlusT ) : _) i_stk = reduce_7 s_stk t_stk i_stk
state_4 s_stk t_stk@(PIToken ( ClParenT ) : _) i_stk = reduce_7 s_stk t_stk i_stk
state_4 s_stk t_stk@(PIToken ( MinusT ) : _) i_stk = reduce_7 s_stk t_stk i_stk
state_4 s_stk t_stk@(PIToken ( StarT ) : _) i_stk = reduce_7 s_stk t_stk i_stk
state_4 _ _ _ = Left "no transition in state 4"

state_5 s_stk (tok@(PIToken ( MinusT )) : t_stk) i_stk = trace "state5, shift5" $ goOn (wrap state_5 : s_stk) t_stk (tok : i_stk)
state_5 s_stk (tok@(PIToken ( OpParenT )) : t_stk) i_stk = trace "state5, shift6" $ goOn (wrap state_6 : s_stk) t_stk (tok : i_stk)
state_5 s_stk (tok@(PIToken ( ValT _ )) : t_stk) i_stk = trace "state5, shift7" $ goOn (wrap state_7 : s_stk) t_stk (tok : i_stk)
state_5 s_stk (PINT4 _ : t_stk) i_stk = trace "state5, goto12" $ goOn (wrap state_12 : s_stk) t_stk i_stk
state_5 _ _ _ = Left "no transition in state 5"

state_6 s_stk (tok@(PIToken ( MinusT )) : t_stk) i_stk = trace "state6, shift5" $ goOn (wrap state_5 : s_stk) t_stk (tok : i_stk)
state_6 s_stk (tok@(PIToken ( OpParenT )) : t_stk) i_stk = trace "state6, shift6" $ goOn (wrap state_6 : s_stk) t_stk (tok : i_stk)
state_6 s_stk (tok@(PIToken ( ValT _ )) : t_stk) i_stk = trace "state6, shift7" $ goOn (wrap state_7 : s_stk) t_stk (tok : i_stk)
state_6 s_stk (PINT1 _ : t_stk) i_stk = trace "state6, goto13" $ goOn (wrap state_13 : s_stk) t_stk i_stk
state_6 s_stk (PINT2 _ : t_stk) i_stk = trace "state6, goto2" $ goOn (wrap state_2 : s_stk) t_stk i_stk
state_6 s_stk (PINT3 _ : t_stk) i_stk = trace "state6, goto3" $ goOn (wrap state_3 : s_stk) t_stk i_stk
state_6 s_stk (PINT4 _ : t_stk) i_stk = trace "state6, goto4" $ goOn (wrap state_4 : s_stk) t_stk i_stk
state_6 _ _ _ = Left "no transition in state 6"

state_7 s_stk t_stk@[] i_stk = reduce_10 s_stk t_stk i_stk
state_7 s_stk t_stk@(PIToken ( HatT ) : _) i_stk = reduce_10 s_stk t_stk i_stk
state_7 s_stk t_stk@(PIToken ( PlusT ) : _) i_stk = reduce_10 s_stk t_stk i_stk
state_7 s_stk t_stk@(PIToken ( ClParenT ) : _) i_stk = reduce_10 s_stk t_stk i_stk
state_7 s_stk t_stk@(PIToken ( MinusT ) : _) i_stk = reduce_10 s_stk t_stk i_stk
state_7 s_stk t_stk@(PIToken ( StarT ) : _) i_stk = reduce_10 s_stk t_stk i_stk
state_7 _ _ _ = Left "no transition in state 7"

state_8 s_stk (tok@(PIToken ( MinusT )) : t_stk) i_stk = trace "state8, shift5" $ goOn (wrap state_5 : s_stk) t_stk (tok : i_stk)
state_8 s_stk (tok@(PIToken ( OpParenT )) : t_stk) i_stk = trace "state8, shift6" $ goOn (wrap state_6 : s_stk) t_stk (tok : i_stk)
state_8 s_stk (tok@(PIToken ( ValT _ )) : t_stk) i_stk = trace "state8, shift7" $ goOn (wrap state_7 : s_stk) t_stk (tok : i_stk)
state_8 s_stk (PINT2 _ : t_stk) i_stk = trace "state8, goto14" $ goOn (wrap state_14 : s_stk) t_stk i_stk
state_8 s_stk (PINT3 _ : t_stk) i_stk = trace "state8, goto3" $ goOn (wrap state_3 : s_stk) t_stk i_stk
state_8 s_stk (PINT4 _ : t_stk) i_stk = trace "state8, goto4" $ goOn (wrap state_4 : s_stk) t_stk i_stk
state_8 _ _ _ = Left "no transition in state 8"

state_9 s_stk (tok@(PIToken ( MinusT )) : t_stk) i_stk = trace "state9, shift5" $ goOn (wrap state_5 : s_stk) t_stk (tok : i_stk)
state_9 s_stk (tok@(PIToken ( OpParenT )) : t_stk) i_stk = trace "state9, shift6" $ goOn (wrap state_6 : s_stk) t_stk (tok : i_stk)
state_9 s_stk (tok@(PIToken ( ValT _ )) : t_stk) i_stk = trace "state9, shift7" $ goOn (wrap state_7 : s_stk) t_stk (tok : i_stk)
state_9 s_stk (PINT2 _ : t_stk) i_stk = trace "state9, goto15" $ goOn (wrap state_15 : s_stk) t_stk i_stk
state_9 s_stk (PINT3 _ : t_stk) i_stk = trace "state9, goto3" $ goOn (wrap state_3 : s_stk) t_stk i_stk
state_9 s_stk (PINT4 _ : t_stk) i_stk = trace "state9, goto4" $ goOn (wrap state_4 : s_stk) t_stk i_stk
state_9 _ _ _ = Left "no transition in state 9"

state_10 s_stk (tok@(PIToken ( MinusT )) : t_stk) i_stk = trace "state10, shift5" $ goOn (wrap state_5 : s_stk) t_stk (tok : i_stk)
state_10 s_stk (tok@(PIToken ( OpParenT )) : t_stk) i_stk = trace "state10, shift6" $ goOn (wrap state_6 : s_stk) t_stk (tok : i_stk)
state_10 s_stk (tok@(PIToken ( ValT _ )) : t_stk) i_stk = trace "state10, shift7" $ goOn (wrap state_7 : s_stk) t_stk (tok : i_stk)
state_10 s_stk (PINT3 _ : t_stk) i_stk = trace "state10, goto16" $ goOn (wrap state_16 : s_stk) t_stk i_stk
state_10 s_stk (PINT4 _ : t_stk) i_stk = trace "state10, goto4" $ goOn (wrap state_4 : s_stk) t_stk i_stk
state_10 _ _ _ = Left "no transition in state 10"

state_11 s_stk (tok@(PIToken ( MinusT )) : t_stk) i_stk = trace "state11, shift5" $ goOn (wrap state_5 : s_stk) t_stk (tok : i_stk)
state_11 s_stk (tok@(PIToken ( OpParenT )) : t_stk) i_stk = trace "state11, shift6" $ goOn (wrap state_6 : s_stk) t_stk (tok : i_stk)
state_11 s_stk (tok@(PIToken ( ValT _ )) : t_stk) i_stk = trace "state11, shift7" $ goOn (wrap state_7 : s_stk) t_stk (tok : i_stk)
state_11 s_stk (PINT3 _ : t_stk) i_stk = trace "state11, goto17" $ goOn (wrap state_17 : s_stk) t_stk i_stk
state_11 s_stk (PINT4 _ : t_stk) i_stk = trace "state11, goto4" $ goOn (wrap state_4 : s_stk) t_stk i_stk
state_11 _ _ _ = Left "no transition in state 11"

state_12 s_stk t_stk@[] i_stk = reduce_8 s_stk t_stk i_stk
state_12 s_stk t_stk@(PIToken ( HatT ) : _) i_stk = reduce_8 s_stk t_stk i_stk
state_12 s_stk t_stk@(PIToken ( PlusT ) : _) i_stk = reduce_8 s_stk t_stk i_stk
state_12 s_stk t_stk@(PIToken ( ClParenT ) : _) i_stk = reduce_8 s_stk t_stk i_stk
state_12 s_stk t_stk@(PIToken ( MinusT ) : _) i_stk = reduce_8 s_stk t_stk i_stk
state_12 s_stk t_stk@(PIToken ( StarT ) : _) i_stk = reduce_8 s_stk t_stk i_stk
state_12 _ _ _ = Left "no transition in state 12"

state_13 s_stk (tok@(PIToken ( PlusT )) : t_stk) i_stk = trace "state13, shift8" $ goOn (wrap state_8 : s_stk) t_stk (tok : i_stk)
state_13 s_stk (tok@(PIToken ( MinusT )) : t_stk) i_stk = trace "state13, shift9" $ goOn (wrap state_9 : s_stk) t_stk (tok : i_stk)
state_13 s_stk (tok@(PIToken ( ClParenT )) : t_stk) i_stk = trace "state13, shift18" $ goOn (wrap state_18 : s_stk) t_stk (tok : i_stk)
state_13 _ _ _ = Left "no transition in state 13"

state_14 s_stk (tok@(PIToken ( StarT )) : t_stk) i_stk = trace "state14, shift10" $ goOn (wrap state_10 : s_stk) t_stk (tok : i_stk)
state_14 s_stk t_stk@[] i_stk = reduce_1 s_stk t_stk i_stk
state_14 s_stk t_stk@(PIToken ( PlusT ) : _) i_stk = reduce_1 s_stk t_stk i_stk
state_14 s_stk t_stk@(PIToken ( ClParenT ) : _) i_stk = reduce_1 s_stk t_stk i_stk
state_14 s_stk t_stk@(PIToken ( MinusT ) : _) i_stk = reduce_1 s_stk t_stk i_stk
state_14 _ _ _ = Left "no transition in state 14"

state_15 s_stk (tok@(PIToken ( StarT )) : t_stk) i_stk = trace "state15, shift10" $ goOn (wrap state_10 : s_stk) t_stk (tok : i_stk)
state_15 s_stk t_stk@[] i_stk = reduce_2 s_stk t_stk i_stk
state_15 s_stk t_stk@(PIToken ( PlusT ) : _) i_stk = reduce_2 s_stk t_stk i_stk
state_15 s_stk t_stk@(PIToken ( ClParenT ) : _) i_stk = reduce_2 s_stk t_stk i_stk
state_15 s_stk t_stk@(PIToken ( MinusT ) : _) i_stk = reduce_2 s_stk t_stk i_stk
state_15 _ _ _ = Left "no transition in state 15"

state_16 s_stk t_stk@[] i_stk = reduce_4 s_stk t_stk i_stk
state_16 s_stk t_stk@(PIToken ( PlusT ) : _) i_stk = reduce_4 s_stk t_stk i_stk
state_16 s_stk t_stk@(PIToken ( ClParenT ) : _) i_stk = reduce_4 s_stk t_stk i_stk
state_16 s_stk t_stk@(PIToken ( MinusT ) : _) i_stk = reduce_4 s_stk t_stk i_stk
state_16 s_stk t_stk@(PIToken ( StarT ) : _) i_stk = reduce_4 s_stk t_stk i_stk
state_16 _ _ _ = Left "no transition in state 16"

state_17 s_stk t_stk@[] i_stk = reduce_6 s_stk t_stk i_stk
state_17 s_stk t_stk@(PIToken ( PlusT ) : _) i_stk = reduce_6 s_stk t_stk i_stk
state_17 s_stk t_stk@(PIToken ( ClParenT ) : _) i_stk = reduce_6 s_stk t_stk i_stk
state_17 s_stk t_stk@(PIToken ( MinusT ) : _) i_stk = reduce_6 s_stk t_stk i_stk
state_17 s_stk t_stk@(PIToken ( StarT ) : _) i_stk = reduce_6 s_stk t_stk i_stk
state_17 _ _ _ = Left "no transition in state 17"

state_18 s_stk t_stk@[] i_stk = reduce_9 s_stk t_stk i_stk
state_18 s_stk t_stk@(PIToken ( HatT ) : _) i_stk = reduce_9 s_stk t_stk i_stk
state_18 s_stk t_stk@(PIToken ( PlusT ) : _) i_stk = reduce_9 s_stk t_stk i_stk
state_18 s_stk t_stk@(PIToken ( ClParenT ) : _) i_stk = reduce_9 s_stk t_stk i_stk
state_18 s_stk t_stk@(PIToken ( MinusT ) : _) i_stk = reduce_9 s_stk t_stk i_stk
state_18 s_stk t_stk@(PIToken ( StarT ) : _) i_stk = reduce_9 s_stk t_stk i_stk
state_18 _ _ _ = Left "no transition in state 18"

reduce_0 s_stk t_stk (PINT1 v0 : i_stk) = unwrap s s_stk' (new_i : t_stk) i_stk'
  where s_stk'@(s : _) = drop 1 s_stk
        i_stk' = new_i : i_stk
        new_i = PINT0 ( v0 )

reduce_1 s_stk t_stk (PINT2 v2 : PIToken v1 : PINT1 v0 : i_stk) = unwrap s s_stk' (new_i : t_stk) i_stk'
  where s_stk'@(s : _) = drop 3 s_stk
        i_stk' = new_i : i_stk
        new_i = PINT1 ( v0 :+ v2 )

reduce_2 s_stk t_stk (PINT2 v2 : PIToken v1 : PINT1 v0 : i_stk) = unwrap s s_stk' (new_i : t_stk) i_stk'
  where s_stk'@(s : _) = drop 3 s_stk
        i_stk' = new_i : i_stk
        new_i = PINT1 ( v0 :- v2 )

reduce_3 s_stk t_stk (PINT2 v0 : i_stk) = unwrap s s_stk' (new_i : t_stk) i_stk'
  where s_stk'@(s : _) = drop 1 s_stk
        i_stk' = new_i : i_stk
        new_i = PINT1 ( v0 )

reduce_4 s_stk t_stk (PINT3 v2 : PIToken v1 : PINT2 v0 : i_stk) = unwrap s s_stk' (new_i : t_stk) i_stk'
  where s_stk'@(s : _) = drop 3 s_stk
        i_stk' = new_i : i_stk
        new_i = PINT2 ( v0 :* v2 )

reduce_5 s_stk t_stk (PINT3 v0 : i_stk) = unwrap s s_stk' (new_i : t_stk) i_stk'
  where s_stk'@(s : _) = drop 1 s_stk
        i_stk' = new_i : i_stk
        new_i = PINT2 ( v0 )

reduce_6 s_stk t_stk (PINT3 v2 : PIToken v1 : PINT4 v0 : i_stk) = unwrap s s_stk' (new_i : t_stk) i_stk'
  where s_stk'@(s : _) = drop 3 s_stk
        i_stk' = new_i : i_stk
        new_i = PINT3 ( v0 :^ v2 )

reduce_7 s_stk t_stk (PINT4 v0 : i_stk) = unwrap s s_stk' (new_i : t_stk) i_stk'
  where s_stk'@(s : _) = drop 1 s_stk
        i_stk' = new_i : i_stk
        new_i = PINT3 ( v0 )

reduce_8 s_stk t_stk (PINT4 v1 : PIToken v0 : i_stk) = unwrap s s_stk' (new_i : t_stk) i_stk'
  where s_stk'@(s : _) = drop 2 s_stk
        i_stk' = new_i : i_stk
        new_i = PINT4 ( Neg v1 )

reduce_9 s_stk t_stk (PIToken v2 : PINT1 v1 : PIToken v0 : i_stk) = unwrap s s_stk' (new_i : t_stk) i_stk'
  where s_stk'@(s : _) = drop 3 s_stk
        i_stk' = new_i : i_stk
        new_i = PINT4 ( v1 )

reduce_10 s_stk t_stk (PIToken ( ValT v0 ) : i_stk) = unwrap s s_stk' (new_i : t_stk) i_stk'
  where s_stk'@(s : _) = drop 1 s_stk
        i_stk' = new_i : i_stk
        new_i = PINT4 ( Val v0 )

