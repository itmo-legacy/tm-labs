{-# LANGUAGE BangPatterns #-}

module Utils where

import           System.IO.Unsafe (unsafePerformIO)


infixl 1 |>
(|>) :: (Functor m) => m a -> (a -> b) -> m b
(|>) = flip (<$>)

infixl 1 &!
(&!) :: a -> (a -> b) -> b
(&!) = flip ($!)

debug :: String -> t -> t
debug msg s = unsafePerformIO (putStrLn msg) `seq` s
