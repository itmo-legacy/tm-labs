{-# LANGUAGE LambdaCase #-}

module Lib
    ( parseExpression
    , eval
    , makeDotText
    ) where

import           Control.Arrow         ((>>>))
import qualified Data.ByteString.Char8 as BS (pack)
import           Data.Function         ((&))
import           Data.GraphViz         as G (graphElemsToDot, printDotGraph)
import           Data.Text.Lazy        (Text)
import qualified Data.Text.Lazy        as T
import           Text.Printf           (printf)

import           Expression            (Expression (..), eval)
import           Parser (parseArithmetics)
import           Graph                 (exprGraphParams, makeExprGraph)
import           Lexer                 (dyslexScanTokens)
import           Utils                 ((|>))


parseExpression :: String -> Either Text Expression
parseExpression s = T.pack s & dyslexScanTokens >>= parseArithmetics

makeDotText :: Expression -> Text
makeDotText = makeExprGraph >>> (\(vs, es) -> G.graphElemsToDot exprGraphParams vs es) >>> G.printDotGraph
