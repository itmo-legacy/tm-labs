module Expression (Expression(..), eval) where

infixl 1 :+
infixl 1 :-
infixl 2 :*

data Expression = Expression :+ Expression
                | Expression :- Expression
                | Expression :* Expression
                | Expression :^ Expression
                | Neg Expression
                | Val Int
                deriving (Show, Eq)

eval :: Expression -> Int
eval (a :+ b) = eval a + eval b
eval (a :- b) = eval a - eval b
eval (a :* b) = eval a * eval b
eval (a :^ b) = eval a ^ eval b
eval (Neg a)  = -(eval a)
eval (Val x)  = x
