{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}
{-# OPTIONS_GHC -fno-warn-unused-matches #-}


module Lexer where

import Data.Text.Lazy (unpack)

import Text.Regex.TDFA ((=~~))
import qualified Data.Text.Lazy as TL



matchFromStart :: TL.Text -> TL.Text -> Maybe (TL.Text, TL.Text)
matchFromStart string regex = string =~~ regex' >>= \(before, match, rest) ->
  if TL.null before
    then return (match, rest)
    else Nothing
  where regex' = TL.cons '^' regex

dyslexScanTokens str | TL.null str = return []
dyslexScanTokens str | Just (match, rest) <- matchFromStart str "\\+" = (( const PlusT ) match :) <$> dyslexScanTokens rest
dyslexScanTokens str | Just (match, rest) <- matchFromStart str "-" = (( const MinusT ) match :) <$> dyslexScanTokens rest
dyslexScanTokens str | Just (match, rest) <- matchFromStart str "\\*" = (( const StarT ) match :) <$> dyslexScanTokens rest
dyslexScanTokens str | Just (match, rest) <- matchFromStart str "\\(" = (( const OpParenT ) match :) <$> dyslexScanTokens rest
dyslexScanTokens str | Just (match, rest) <- matchFromStart str "\\)" = (( const ClParenT ) match :) <$> dyslexScanTokens rest
dyslexScanTokens str | Just (match, rest) <- matchFromStart str "\\^" = (( const HatT ) match :) <$> dyslexScanTokens rest
dyslexScanTokens str | Just (match, rest) <- matchFromStart str "[0-9]+" = (( ValT . read . unpack ) match :) <$> dyslexScanTokens rest
dyslexScanTokens str | Just (match, rest) <- matchFromStart str " +" = dyslexScanTokens rest
dyslexScanTokens str = Left $ "Failed to lex, starting from '" <> str <> "'"


data Token
    = PlusT
    | MinusT
    | StarT
    | HatT
    | OpParenT
    | ClParenT
    | ValT Int
    deriving (Eq, Show)

