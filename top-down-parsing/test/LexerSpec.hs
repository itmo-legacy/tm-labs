{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module LexerSpec (spec) where

import qualified Data.ByteString.Char8 as BS

import           Test.Hspec
import           Test.QuickCheck

import           Lexer

spec :: Spec
spec = do
  describe "Lexer.tokenize" $ do
    it "breaks arithmetic expressions into tokens" $ do
      tokenize "(1+2)*3" `shouldBe` return [OpParenT, ValT 1, PlusT, ValT 2, ClParenT, StarT, ValT 3]

    it "lexes numbers to ValT" $ do
      property $ \(Positive x) -> tokenize (BS.pack $ show x) `shouldBe` return [ValT x]

    it "ignores whitespaces" $ do
      tokenize "  - (\t 8 * 9)  - 123 *0   "
        `shouldBe` return
        [MinusT, OpParenT, ValT 8, StarT, ValT 9, ClParenT, MinusT, ValT 123, StarT, ValT 0]
