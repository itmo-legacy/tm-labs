module ExpressionParserSpec (spec) where

import           Test.Hspec
import           Test.QuickCheck

import           Expression       (Expression (..))
import           ExpressionParser
import           Lexer            (tokenize)
import           Lib              (parseExpression)
import           Utils            ((|>))

parse :: String -> Either String Expression
parse = parseExpression

spec :: Spec
spec = do
  describe "ExpressionParser" $ do
    it "parses +/- left to right" $ do
      parse "8 + 9 - 8 + 5" `shouldBe` return (v 8 :+ v 9 :- v 8 :+ v 5)

    it "parses * with greater priority than +/-" $ do
      parse "9+7 * 6+4" `shouldBe` return (v 9 :+ v 7 :* v 6 :+ v 4)

    it "gives expressions in parentheses higher priority" $ do
      parse "(1 + 3) * 8" `shouldBe` return ((v 1 :+ v 3) :* v 8)

    it "supports negation mixed with other operations" $ do
      parse "-8 + - 3 * - 6 * -(1 + 3)" `shouldBe` return (nv 8 :+ nv 3 :* nv 6 :* n (v 1 :+ v 3))

  where v = Val
        n = Neg
        nv = n . v
