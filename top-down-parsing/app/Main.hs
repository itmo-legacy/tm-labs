{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Data.Either (either)
import           Data.Foldable (for_)
import qualified Data.Text.Lazy as T
import qualified Data.Text.Lazy.IO as T
import           System.Process (callCommand)

import           Lib
import           Utils ((|>))

main :: IO ()
main = do
  parsedLines <- getContents |> lines |> map parseExpression
  for_ parsedLines $ either
                      (\msg -> T.putStrLn $ "Oops! " <> msg)
                      (\parsed -> do
                          print (eval parsed)
                          let dotText = T.unpack $ makeDotText parsed
                          callCommand (showPdfCommand dotText)
                      )
  where showPdfCommand dotText = "echo '" ++ dotText ++ "' | dot -T pdf | zathura - &>/dev/null"
