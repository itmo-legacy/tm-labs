#+TITLE: Report
#+AUTHOR: Artem Yurchenko

$$ \newpage $$
* Grammar
** Simple grammar
   + E → T + E
   + E → T - E
   + E → T
   + T → F * T
   + T → F
   + F → - F
   + F → ( E )
   + F → n

** LL grammar
   + E  → T E'
   + E' → + T E'
   + E' → \ndash T E'
   + E' → \epsilon
   + T  → F T'
   + T' → * F T'
   + T' → \epsilon
   + F  → \ndash F
   + F  → ( E )
   + F  → n

   $$ \newpage $$
* FIRST and FOLLOW
  (for LL grammar)

  #+ATTR_LATEX: :environment longtable :align |l|p{2cm}|p{2cm}|
  | NT | FIRST   | FOLLOW        |
  |----+---------+---------------|
  | E  | \ndash, (, n | ), $          |
  | E' | +, \ndash, \epsilon | ), $          |
  | T  | \ndash, (, n | +, \ndash, ), $    |
  | T' | *, \epsilon    | +, \ndash, ), $    |
  | F  | \ndash, (, n | *, +, -, ), $ |
