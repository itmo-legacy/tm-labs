module ExLR0 (Ast (..), eval) where

data Ast = Plus Ast Ast | Minus Ast Ast | Number Int deriving Show

eval :: Ast -> Int
eval (Plus e1 e2) = eval e1 + eval e2
eval (Minus e1 e2) = eval e1 - eval e2
eval (Number i) = i
