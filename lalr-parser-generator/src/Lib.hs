module Lib (removeExtension) where

import           Data.List (isSuffixOf)

removeExtension :: String -> String -> Maybe String
removeExtension ext filename
  | ext `isSuffixOf` filename = return $ take (length filename - length ext) filename
  | otherwise = Nothing
