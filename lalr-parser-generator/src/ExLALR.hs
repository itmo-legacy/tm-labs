module ExLALR (Expr (..), Value (..)) where

import           Data.Text.Lazy (Text)

data Expr = Assignment Value Value | Statement Value deriving Show

data Value = Ptr Value | Var Text deriving Show
