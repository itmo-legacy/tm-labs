{-# LANGUAGE OverloadedStrings #-}

module DummyUtils where

import           Control.Monad (foldM)
import           Data.Functor ((<&>))
import           Data.Maybe (listToMaybe)
import           Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as T
import           Debug.Trace

replaceDollarDollar :: Text -> Text -> Text
replaceDollarDollar code var
  | T.null after = var
  | otherwise = T.concat ["(", before, var, T.drop 2 after, ")"]
  where (before, after) = T.breakOn "$$" code

tShow :: Show a => a -> Text
tShow = T.pack . show

breakM :: (Foldable t, Monad m) => (a -> m Bool) -> t a -> m ([a], [a])
breakM pr = foldM folder ([], [])
  where folder (l, r) x = pr x <&> (\p -> if p then (x : l, r) else (l, x : r))

lookup3 :: Eq a => a -> [(a, b, c)] -> Maybe (b, c)
lookup3 key l = listToMaybe [(b, c) | (a, b, c) <- l, key == a]

fst3 :: (a, b, c) -> a
fst3 (a, _, _) = a

snd3 :: (a, b, c) -> b
snd3 (_, b, _) = b

trd3 :: (a, b, c) -> c
trd3 (_, _, c) = c

uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (a, b, c) = f a b c

uncurry4 :: (a -> b -> c -> d -> e) -> (a, b, c, d) -> e
uncurry4 f (a, b, c, d) = f a b c d

traceAndUse :: Show a => a -> a
traceAndUse a = trace (show a) a

maybeToEither :: a -> Maybe b -> Either a b
maybeToEither l = maybe (Left l) Right
