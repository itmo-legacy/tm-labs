{
{-# LANGUAGE OverloadedStrings #-}

module DyslexParser (parseDyslex, Rule (..)) where

import           Data.Maybe      (catMaybes)
import           Data.Text.Lazy  (Text)

import DyslexLexer
}

%name       parseDyslex
%tokentype  { Token }
%error      { fail "parse error" }
%monad      { Either Text } { >>= } { return }

%token BIND      { BindT }
%token SEMICOLON { SemicolonT }
%token STRING    { StringT $$ }
%token CODE      { CodeT $$ }
%token REGEX     { RegexT $$ }

%%

dyslex : CODE BIND rules CODE { ($1, $3, $4) }

rules : rule rules            { $1 : $2 }
      | {- empty -}           { [] }

rule : regex tokenCtor        { Rule $1 $2 }

regex : STRING                { $1 }
      | REGEX                 { $1 }

tokenCtor : SEMICOLON         { Nothing }
          | CODE              { Just $1 }

{
data Rule = Rule Text (Maybe Text) deriving Show
}
