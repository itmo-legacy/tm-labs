{
module DummyLexer (alexScanTokens, Token (..)) where

import           Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as T (pack)
}

%wrapper "basic"

$alpha = [a-zA-Z]
$digit = [0-9]
$idchar = [$alpha $digit \_ \']

@ws = $white+
@id = $alpha $idchar*
@word = [^$white]+

tokens :-

      @ws                   ;
      \{(\n | [^\}])*\}     { CodeT . T.pack . tail . init }
      \%name                { const NameDirT }
      \%tokentype           { const TokentypeDirT }
      \%token               { const TokenDirT }

      \{ .* \}              { CodeT . T.pack . tail . init }
      :                     { const ColonT }
      \%\%                  { const PercentPercentT }
      @word                 { WordT . T.pack }

{
data Token = NameDirT | TokentypeDirT | TokenDirT
           | ColonT | PercentPercentT
           | IdT Text | CodeT Text | WordT Text
  deriving Show
}
