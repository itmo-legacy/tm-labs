{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Dyslex where

import           Data.String.Interpolation (str)
import           Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as T

import           DyslexLexer (alexScanTokens)
import           DyslexParser (Rule (..), parseDyslex)

generateLexer :: String -> Either Text Text
generateLexer dsl = do
  (importCode, rules, declCode) <- parseDyslex $ alexScanTokens dsl
  return $ T.unlines [pragmas, importCode, imports, "\n", regexFunction, lexerFunction rules, declCode]
  where
    pragmas = T.unlines [ "{-# LANGUAGE OverloadedStrings #-}"
                        , "{-# OPTIONS_GHC -fno-warn-missing-signatures #-}"
                        , "{-# OPTIONS_GHC -fno-warn-unused-matches #-}"
                        ]
    imports = T.unlines [ "import Text.Regex.TDFA ((=~~))"
                        , "import qualified Data.Text.Lazy as TL"
                        ]

    regexFName = "matchFromStart"
    regexFunction =
      [str|$regexFName$ :: TL.Text -> TL.Text -> Maybe (TL.Text, TL.Text)
           $regexFName$ string regex = string =~~ regex' >>= \(before, match, rest) ->
             if TL.null before
               then return (match, rest)
               else Nothing
             where regex' = TL.cons '^' regex
           |]

    lexerFName = "dyslexScanTokens"
    lexerFunction rules = T.unlines ([baseCase] ++ map ruleToCode rules ++ [defaultCase])

    baseCase = [str|$lexerFName$ str | TL.null str = return []|]
    ruleToCode (Rule regex mCtor) = [str|$lexerFName$ str | Just (match, rest) <- $regexFName$ str "$regex$" = $ctorCode$$lexerFName$ rest|]
      where ctorCode = maybe "" (\ctor -> [str|(($ctor$) match :) <$$> |]) mCtor
    defaultCase = [str|$lexerFName$ str = Left $$ "Failed to lex, starting from '" <> str <> "'"|]
