{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}

module Dummy (generateParser) where


import           Control.Arrow (second, (>>>))
import           Control.Monad (join)
import           Data.Either (partitionEithers)
import           Data.Function ((&))
import           Data.Functor ((<&>))
import qualified Data.HashMap.Strict as M
import           Data.List (elemIndex, foldl', nub, zip3)
import           Data.Maybe (fromJust)
import           Data.String.Interpolation (str)
import           Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as T
import           Debug.Trace (trace)
import           DummyLexer (alexScanTokens)
import           DummyParser (NTBind, TokBind, parseDummy)
import           DummyUtils (fst3, maybeToEither, replaceDollarDollar, trd3, uncurry3)
import           Lalr (LR1ISet, LR1Item (..), LR1State (..), NT (..), Rule, Symbol (..), Token (..),
                       TransSet, makeLALRAutomaton, showLR1ISet, showLR1Item)


dataToRules :: [TokBind] -> [NTBind] -> Either Text [Rule]
dataToRules tokBinds ntBinds = mapM toRule ntBinds
  where toks = map fst tokBinds & flip zip [0..]
        nts = map fst3 ntBinds & nub & flip zip [0..]
        toRule (lhs, prod, _) = (,) <$> toNT lhs <*> mapM toSymbol prod

        toNT word | Just i <- word `lookup` nts = return $ NT i word
                  | otherwise = Left [str|$word$ is not a nonterminal|]

        toTok word | Just i <- word `lookup` toks = return $ Token i word
                   | otherwise = Left [str|$word$ is not a terminal|]

        toSymbol word = (SymNT <$> toNT word) <> (SymTok <$> toTok word)
                     <> Left [str|$word$ is neither a terminal nor a nonterminal|]

generateParser :: String -> Either Text Text
generateParser dsl = do
  (importCode, parserName, tokenType, tokBinds, ntBinds) <- parseDummy $ alexScanTokens dsl
  let ntsCount = map fst3 ntBinds & nub & length
  let codes = map trd3 ntBinds
  rules <- dataToRules tokBinds ntBinds
  (itemSets, transSets) <- makeLALRAutomaton rules <&> map (\(LR1State a b) -> (a, b)) <&> unzip
  reduces <- mapM (extractReduce rules) itemSets
  trace (itemSets & map showLR1ISet & unlines) $! return $ T.unlines $
    [ genPragmas
    , importCode
    , genParserImports <> "\n"
    , genWrapper <> "\n"
    , genParsedItemData ntsCount tokenType <> "\n"
    , genParseFunction parserName <> "\n"
    , genGoOnFunction <> "\n"
    ]
    ++ genStates tokBinds transSets reduces
    ++ genReduces tokBinds codes rules

type TransNT = [(Int, Int)]
type TransTok = [(Text, Int)]
type Reduce = [(Maybe Text, Int)]

genPragmas :: Text
genPragmas = T.unlines [ "{-# LANGUAGE OverloadedStrings #-}"
                       , "{-# OPTIONS_GHC -fno-warn-missing-signatures #-}"
                       , "{-# OPTIONS_GHC -fno-warn-unused-matches #-}"
                       ]

genParserImports :: Text
genParserImports = T.unlines [ "import qualified Data.Text.Lazy as T (Text, pack)"
                             , "import Debug.Trace (trace)"
                             ]

genWrapper :: Text
genWrapper = [str|newtype Wrapper a b = Wrapper {unwrap :: [Wrapper a b] -> [a] -> [a] -> Either T.Text b}
                  wrap = Wrapper|]

genParsedItemData :: Int -> Text -> Text
genParsedItemData ntsCount tokenType = T.append dataDecl (T.intercalate " | " valueCtors)
  where dataDecl = [str|data ParsedItem #i in [0..ntsCount]:t$:i$| # = |]
        valueCtors = tokenCtor : ntCtors
        tokenCtor = [str|PIToken ($tokenType$)|]
        ntCtors = map (\i -> [str|PINT$:i$ t$:i$|]) [0..ntsCount]

genParseFunction :: Text -> Text
genParseFunction parserName = [str|$parserName$ tokens = goOn [wrap state_0] (map PIToken tokens) []|]

genGoOnFunction :: Text
genGoOnFunction = [str|goOn s_stk@(s : _) t_stk i_stk = unwrap s s_stk t_stk i_stk
                       goOn _ _ _ = Left (T.pack "can't keep on parsing")|]

genStates :: [TokBind] -> [TransSet] -> [Reduce] -> [Text]
genStates tokBinds transSets reduces =
  map (uncurry3 $ genState tokBinds) (zip3 transSets reduces [0 ..])

genReduces :: [TokBind] -> [Text] -> [Rule] -> [Text]
genReduces tokBinds codes rules = map (uncurry3 $ genReduce tokBinds) (zip3 codes rules [0..])

genState :: [TokBind] -> TransSet -> Reduce -> Int -> Text
genState tokBinds trans reduce i =
  T.unlines $ concat [ genAcceptState
                     , genShiftCases
                     , genGotoCases
                     , genReduceCases
                     , genDefaultCase
                     ]
  where
    genAcceptState
      | i == 0 = return [str|state_$:i$ [_] [PINT0 _] [PINT0 v] = return v|]
      | otherwise = []

    genShiftCases = map genShiftCase action
    genShiftCase (tok, ni) = [str|state_$:i$ s_stk (tok@(PIToken ($bind tok$)) : t_stk) i_stk = trace "state$:i$, shift$:ni$" $$ goOn (wrap state_$:ni$ : s_stk) t_stk (tok : i_stk)|]

    genGotoCases = map genGotoCase goto
    genGotoCase (ntI, ni) = [str|state_$:i$ s_stk (PINT$:ntI$ _ : t_stk) i_stk = trace "state$:i$, goto$:ni$" $$ goOn (wrap state_$:ni$ : s_stk) t_stk i_stk|]

    genReduceCases = map genReduceCase reduce
    genReduceCase (mtok, redI) =
      [str|state_$:i$ s_stk t_stk@$pat$ i_stk = reduce_$:redI$ s_stk t_stk i_stk|]
        where
          pat = maybe "[]" match mtok
          match tok = [str|(PIToken ($bind tok$) : _)|]

    genDefaultCase = return [str|state_$:i$ _ _ _ = Left "no transition in state $:i$"|]

    bind tok = T.replace "$$" "_" (fromJust $ lookup tok tokBinds)

    (action, goto) = extractActionAndGoto trans

genReduce :: [TokBind] -> Text -> Rule -> Int -> Text
genReduce tokBinds preCode (NT redI _, prod) i =
  [str|reduce_$:i$ s_stk t_stk $itemsMatching$ = unwrap s s_stk' (new_i : t_stk) i_stk'
       $"  "$where s_stk'@(s : _) = drop $:n$ s_stk
               i_stk' = new_i : i_stk
               new_i = PINT$:redI$ ($code$)|] <> "\n"
  where
    itemsMatching = [str|(#item in (reverse items):$item$| : #)|]
    items = "i_stk" : zipWith wrapItem [0..] prod

    n = length prod
    code = foldl' replaceDollar preCode [0..length items - 1]

    wrapItem :: Int -> Symbol -> Text
    wrapItem j (SymTok (Token tokI _)) = [str|PIToken $var$|]
      where var = replaceDollarDollar bind [str|v$:j$|]
            bind = tokBinds !! tokI & snd
    wrapItem j (SymNT (NT ntI _)) = [str|PINT$:ntI$ v$:j$|]

    replaceDollar :: Text -> Int -> Text
    replaceDollar text j = T.replace [str|$$$:j + 1$|] [str|v$:j$|] text


extractActionAndGoto :: TransSet -> (TransTok, TransNT)
extractActionAndGoto = map classify >>> partitionEithers
  where classify (SymNT (NT i _), v)     = Right (i, v)
        classify (SymTok (Token i s), v) = Left (s, v)

extractReduce :: [Rule] -> LR1ISet -> Either Text Reduce
extractReduce rules items =
  [ elemIndex (lhs, reverse done) rules
    <&> makeKV la
    & maybeToEither [str|Can't find reduction rule for $T.pack (showLR1Item item)$|]
  | item@(LR1Item lhs done undone la) <- items
  , null undone
  ] & sequence
  <&> concat
  <&> map (second Right)
  <&> M.fromListWith errorMsg
  <&> sequence & join
  <&> M.toList
  where
    makeKV la ruleI = la & map (fmap getText) & map (, ruleI)
    getText (Token i tok) = tok
    errorMsg _ _ =
      Left $ "reduce-reduce conflict in " <> T.pack (showLR1ISet items)
