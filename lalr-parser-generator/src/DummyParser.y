{
{-# LANGUAGE OverloadedStrings #-}

module DummyParser (parseDummy, TokBind, NTBind) where

import           Data.Maybe      (catMaybes)
import           Data.Text.Lazy  (Text)

import DummyLexer (Token (..))
}

%name parseDummy
%tokentype  { Token }
%error      { fail "parse error" }
%monad      { Either Text } { >>= } { return }

%token
  "%name"       { NameDirT }
  "%tokentype"  { TokentypeDirT }
  "%token"      { TokenDirT }
  ":"           { ColonT }
  "%%"          { PercentPercentT }
  WORD          { WordT $$ }
  CODE          { CodeT $$ }

%%

dummy :: { (Text, Text, Text, [TokBind], [NTBind]) }
      : CODE parserName tokenType terminals "%%" rules { ($1, $2, $3, $4, $6) }

parserName : "%name" WORD         { $2 }

tokenType : "%tokentype" CODE     { $2 }

terminals : "%token" terms_rev    { reverse $2 }

terms_rev : terms_rev WORD CODE   { ($2, $3) : $1 }
          | {- empty -}           { [] }

rules : rules_rev                 { reverse $1 }

rules_rev : rules_rev rule        { $2 : $1 }
          | rule                  { [$1] }

rule : WORD ":" prods CODE        { ($1, reverse $3, $4) }

prods : prods WORD                { $2 : $1 }
      | {- empty -}               { [] }

{
type TokBind = (Text, Text)
type NTBind = (Text, [Text], Text)
}
