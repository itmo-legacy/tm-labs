{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE TupleSections     #-}

module Lalr where

import           Control.Arrow (second)
import           Control.Monad (foldM)
import           Control.Monad.Except (throwError)
import           Control.Monad.State.Lazy (StateT (..), evalStateT, gets, modify)
import           Data.Function (on, (&))
import           Data.Functor ((<&>))
import           Data.Hashable (Hashable)
import           Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HM
import           Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IM
import           Data.List (delete, foldl', intercalate, sort, sortOn, union)
import           Data.Maybe (mapMaybe)
import           Data.String.Interpolation (str)
import           Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as T
import           GHC.Generics (Generic)

import           DummyUtils (fst3, trd3)


data NT = NT Int Text deriving (Eq, Ord, Show, Generic)
data Token = Token Int Text deriving (Eq, Ord, Show, Generic)
data Symbol = SymNT NT | SymTok Token deriving (Eq, Ord, Show, Generic)

instance Hashable Token
instance Hashable NT
instance Hashable Symbol

type TransSet = [(Symbol, Int)]
type Lookahead = [Maybe Token]
type LR0Item = (NT, [Symbol], [Symbol])
type LR0ISet = [LR0Item]
type LALRKV = (LR0Item, Lookahead)
type KVSet = [LALRKV]

data LR1Item = LR1Item
    { lr1Lhs       :: NT
    , lr1Done      :: [Symbol] -- done is reversed
    , lr1Undone    :: [Symbol]
    , lr1Lookahead :: Lookahead
    } deriving (Ord, Show)
type LR1ISet = [LR1Item]
data LR1State = LR1State LR1ISet TransSet deriving (Show)
type LALRAutomaton = [LR1State]

lalrKey :: LR1Item -> LR0Item
lalrKey (LR1Item lhs done undone _) = (lhs, done, undone)

makeItem :: LR0Item -> Lookahead -> LR1Item
makeItem (lhs, done, undone) la = LR1Item lhs done undone la

instance Eq LR1Item where
  (==) = (==) `on` ((,,) <$> lr1Lhs <*> lr1Done <*> lr1Undone)

showLR1Item :: LR1Item -> String
showLR1Item (LR1Item (NT _ lhs) done undone la) = T.unpack [str|$lhs$ -> $fp$ . $sp$, [$lap$]|]
  where fp = reverse done
           & map (\case SymTok (Token _ s) -> s; SymNT (NT _ s) -> s)
           & T.unwords
        sp = undone
           & map (\case SymTok (Token _ s) -> s; SymNT (NT _ s) -> s)
           & T.unwords
        lap = T.intercalate " | " $ map (maybe "$" (\(Token _ s) -> s)) la

showLR1ISet :: LR1ISet -> String
showLR1ISet []      = "{ }"
showLR1ISet (x: xs) = ([str|[ $showLR1Item x$|] : map showLR1Item xs & intercalate "\n, ") ++ "\n]"
type Rule = (NT, [Symbol])

eps :: Maybe a
eps = Nothing

eof :: Maybe a
eof = Nothing

makeLALRAutomaton :: [Rule] -> Either Text LALRAutomaton
makeLALRAutomaton [] = Left [str|Can't build an automaton with no rules|]
makeLALRAutomaton rules@((lhs, rhs) : _) = evalStateT stateful (rules, IM.empty)
  where
    stateful = do
      start <- close ((lhs, [], rhs), [eof])
      let (key, las0) = unzip start
      states <- buildStates (HM.singleton key (0, las0, [])) [] key
      states
        & HM.toList
        & sortOn (fst3 . snd)
        & map (\(bases, (_, las, trans)) -> LR1State (zipWith makeItem bases las) trans)
        & return

type MyState = ([Rule], FirstMemo)
type Stateful = StateT MyState (Either Text)
type ISetMemo = HashMap LR0ISet (Int, [Lookahead], TransSet)

buildStates :: ISetMemo -> [LR0ISet] -> LR0ISet -> Stateful ISetMemo
buildStates memo queue key = do
  let (i, las, _) = memo HM.! key
  transSets <- makeTransSets (zip key las)
  let memo' = calculateTransitions memo (map snd transSets)
  let trans =
        map (second $ \kvset' -> fst3 $ memo' HM.! map fst kvset') transSets &
        sort
  let memo'' = HM.adjust (\(i, las', _) -> (i, las', trans)) key memo'
  let updatedSets = HM.differenceWith diff_f memo'' memo
                   & HM.keys
                   & sortOn (fst3 . (memo'' HM.!))
  let queue' = queue ++ updatedSets
  case queue' of
    []     -> return memo''
    (x:xs) -> buildStates memo'' xs x
  where
    getText (Token _ s) = T.unpack s
    diff_f (i, las1, trans) (_, las2, _)
      | las1 == las2 = Nothing
      | otherwise = Just (i, zipWith joinLA las1 las2, trans)

makeTransSets :: KVSet -> Stateful [(Symbol, KVSet)]
makeTransSets kvset = mapMaybe moveForward kvset
                    & mapM (\(a, b) -> (a, ) <$> close b)
                  <&> HM.fromListWith (++)
                  <&> HM.toList
                  <&> sort

calculateTransitions :: ISetMemo -> [KVSet] -> ISetMemo
calculateTransitions memo' kvsets = foldl' folder memo' kvsets
  where
    folder :: ISetMemo -> KVSet -> ISetMemo
    folder memo kvset
      | Just (i, las, trans) <- HM.lookup key memo =
        HM.insert key (i, zipWith joinLA las las0, trans) memo
      | otherwise = HM.insert key (length memo, las0, []) memo
      where (key, las0) = unzip kvset

close :: LALRKV -> Stateful KVSet
close item' = go HM.empty item'
          <&> HM.toList
          <&> sort
  where
    go acc (key, lookahead)
      | (SymNT nextNT : rest) <- trd3 key, acc' /= acc = do
          first <- getFirstL rest
          let lookahead' = joinSets first lookahead
          rules <- gets fst
          let epsTrans = [((lhs, [], rhs), lookahead') | (lhs, rhs) <- rules, lhs == nextNT]
          foldM go acc' epsTrans
      | otherwise = return acc'
      where
        acc' = HM.insertWith joinLA key lookahead acc

moveForward :: LALRKV -> Maybe (Symbol, LALRKV)
moveForward (key, val) = go key <&> second (, val)
  where
    go (_, _, [])              = Nothing
    go (lhs, done, k : undone) = Just (k, (lhs, k : done, undone))

type FirstSet = [Maybe Token]
type FirstMemo = IntMap FirstSet

getFirst :: Symbol -> Stateful FirstSet -- TODO problems with indirect left recursion
getFirst (SymTok tok) = return [Just tok]
getFirst (SymNT nt@(NT ntI ntS)) = gets snd <&> IM.lookup ntI >>= maybe memoizedCalc return
  where
    memoizedCalc = do
      res <- calc
      modify (second $ IM.insert ntI res)
      return res

    calc = do
      rules <- gets fst
      (res1, epsSet) <- foldM folder ([], []) [prod | (lhs, prod) <- rules, lhs == nt]
      res2 <- concat <$> mapM getFirstL epsSet
      if null res1
        then throwError [str|terminal $ntS$ has an infinite production rule|]
        else return $ joinSets res1 res2

    folder (acc, epsSet) [] = return (acc `union` [eps], epsSet)
    folder (acc, epsSet) (SymTok x : _) = return (acc `union` [Just x], epsSet)
    folder (acc, epsSet) (SymNT x : xs) | x == nt = return (acc, xs : epsSet)
    folder (acc, epsSet) (x : xs) = do
      xFirst <- getFirst x
      if eps `elem` xFirst
        then folder (acc `union` delete eps xFirst, epsSet) xs
        else return (acc `union` xFirst, epsSet)

getFirstL :: [Symbol] -> Stateful FirstSet
getFirstL xs = mapM getFirst xs <&> foldl' joinSets [eps]

joinLA :: Lookahead -> Lookahead -> Lookahead
joinLA a b = sort (a `union` b)

joinSets :: FirstSet -> FirstSet -> FirstSet
joinSets a b | eps `elem` a = delete eps a `union` b
             | otherwise    = a
