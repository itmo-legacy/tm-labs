{
module DyslexLexer where

import           Data.Text.Lazy (Text, pack)
}

%wrapper "basic"

@ws = $white+
@id = [a-zA-Z]*

tokens :-

       @ws                   ;
       \{(\n | [^\}])*\}     { CodeT . pack . tail . init }
       \~.*\~                { RegexT . pack . tail . init }
       @id @ws? \:\-         { const BindT }
       \;                    { const SemicolonT }
       [^\ ]+                { StringT . pack }

{
data Token = BindT | SemicolonT | StringT Text | CodeT Text | RegexT Text deriving Show
}
