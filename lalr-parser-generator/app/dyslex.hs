{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns      #-}

import           Control.Arrow ((>>>))
import           Data.Function ((&))
import qualified Data.Text.Lazy.IO as T
import           System.Environment (getArgs)
import           System.Exit (die)

import           Dyslex (generateLexer)
import           Lib (removeExtension)

main :: IO ()
main = getArgs >>= parse >>= makeLexer

parse :: [String] -> IO String
parse [removeExtension ".dx" -> Just lexer] = return lexer
parse [_]                                   = die "dyslex files shoud have extension '.dx'"
parse _                                     = die "Usage: dyslex file"

makeLexer :: FilePath -> IO ()
makeLexer lexer = do
  lexerDsl <- readFile (lexer <> ".dx")
  generateLexer lexerDsl & either
    (T.putStrLn . ("lexer error: " <>))
    (T.writeFile (lexer <> ".hs") >>> (<* putStrLn "ok"))

