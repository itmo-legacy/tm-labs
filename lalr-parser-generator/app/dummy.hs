{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns      #-}

import           Control.Arrow ((>>>))
import           Data.Function ((&))
import qualified Data.Text.Lazy.IO as T
import           System.Environment (getArgs)
import           System.Exit (die)

import           Dummy (generateParser)
import           Lib (removeExtension)

main :: IO ()
main = getArgs >>= parse >>= makeParser

parse :: [String] -> IO String
parse [removeExtension ".dy" -> Just parser] = return parser
parse [_]                                    = die "dummy files shoud have extension '.dy'"
parse _                                      = die "Usage: dummy file"

makeParser :: FilePath -> IO ()
makeParser parser = do
  parserDsl <- readFile (parser <> ".dy")
  generateParser parserDsl & either
    (T.putStrLn . ("parser error: " <>))
    (T.writeFile (parser <> ".hs") >>> (<* putStrLn "ok"))
